'use strict';

// Declare Marvel API factory
angular.module('marvel-app.marvel', [])

.factory('util', function() {
    return {
        getQueryString: function(params) {
            var result = [];
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    result.push(key + '=' + encodeURIComponent(params[key]));
                }
            }
            return result.join('&');
        },
        range: function(min, max, step) {
            var result = [];
            step = step || 1;
            for (var i = min; i <= max; i += step) {
                result.push(i);
            }
            return result;
        },
        group: function(data, groupSize) {
            var result = [], group = [];
            for (var i = 0, l = data.length; i < l; i++) {
                group.push(data[i]);
                if ((i + 1) % groupSize === 0) {
                    result.push(group);
                    group = [];
                }
            }
            if (group.length > 0) {
                result.push(group);
            }
            return result;
        }
    };
})

.factory('MarvelAPI', function($http, util, md5) {
	
	var GATEWAY = 'https://gateway.marvel.com:443/v1/public/';

	function MarvelAPI(publicKey, privateKey) {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}

    MarvelAPI.prototype.getUrl = function(params, action) {
    	//  CREATE the URL :  MARVEL-STARS-GATEWAY / 'actionMethod' ? [ts + hash(ts+privateKey+publicKey) + publicKey]
        var timeStamp = Math.floor(Date.now() / 1000);
        
        params.ts = timeStamp;
        params.hash = md5.createHash(timeStamp + this.privateKey + this.publicKey);
        params.apikey = this.publicKey;

        return GATEWAY + action +  '?' + util.getQueryString(params);
    };

    MarvelAPI.prototype.getData = function(params, action) {
    	// SET the URL 
        var url = this.getUrl(params, action);
        
        return $http.jsonp(url)
	        .then(function(response) {
	        	console.log('SUCCESS to GET json with action : ' + action);
	        	return response.data.data;
	        })
	        .catch(function(error) {
	        	console.log('########### An error has occured : ', error.message);
	        });
    };


    // Public methods
    MarvelAPI.prototype.getComics = function(from, perPage) {
        var params = { callback: 'JSON_CALLBACK' };
        var action = 'comics';

        if (perPage !== undefined) params.limit = perPage;
        if (from !== undefined) params.offset = from; 

        return this.getData(params, action);
    };    

    MarvelAPI.prototype.getCharacters = function(from, perPage) {
        var params = { callback: 'JSON_CALLBACK' };
        var action = 'characters';

        if (perPage !== undefined) params.limit = perPage;
        if (from !== undefined) params.offset = from; 

        return this.getData(params, action);
    };  

	return MarvelAPI;
});