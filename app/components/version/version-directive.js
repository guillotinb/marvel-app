'use strict';

angular.module('marvel-app.version.version-directive', [])

.directive('appVersion', function(version) {
  	return function(scope, elm, attrs) {
    	elm.text(version);
  	};
});
