'use strict';

angular.module('marvel-app.version', [
  'marvel-app.version.version-directive'
])

.value('version', '0.2');
