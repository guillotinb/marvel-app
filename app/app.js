'use strict';

// DEVELOPER 			: Benoit GUILLOTIN
// DATE 				: 10-Août-2017
// ANGULAR-SEED 1.5 	: https://github.com/angular/angular-seed
// HOMEPAGE				: https://bitbucket.org/guillotinb/marvel-app

// DECLARE APP level module for VIEWS and COMPONENTS
angular.module('marvel-app', [
  'ngRoute',
/*  'ngLoader',*/
  'marvel-app.view1', 
  'marvel-app.view2',
  'marvel-app.version',
  'marvel-app.marvel',
  'angular-md5'
]) 

.constant('FOLDER', {
	/*'HTML_VIEW'     : 'js/view/',
	'IMG'     : 'js/IMG/',*/
	'JSON_TEXT'     : '/'
})
// I usually do not organise Views and Controllers like this. BUT ... 
// EVERY project had is own architecture.
// I DO PREFER : 1 folder for controllers & 1 folder for views & includeViews.
.config(function($locationProvider, $routeProvider, $sceDelegateProvider) {
/*	$sceDelegateProvider.resourceUrlWhitelist([
	    // Allow same origin resource loads.
	    'self',
	    // Allow loading from our assets domain. **.
		'https://gateway.marvel.com:443/*'
	  ]);*/
/*   	delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.useXDomain = true;	*/          

  	$locationProvider.hashPrefix('!')
  	$routeProvider.otherwise({redirectTo: '/view1'});
})
// PUBLIC MARVEL KEY
.constant('MARVEL_PUBLIC_KEY', 'f58c2131a1f5cdc5d8ac6335546b8aef')
// ######################################################################
// TODO --- THIS key should be hide on server-side or already Encrypt as MD5 key by nodeJS
.constant('MARVEL_PRIVATE_KEY', '30884efe88f8f0f49a8b09b4dee4f2f8842ee5e1')
// ######################################################################
// MARVELLOUS FACTORY
.factory('marvel', function(MarvelAPI, MARVEL_PUBLIC_KEY, MARVEL_PRIVATE_KEY) {
    return new MarvelAPI(MARVEL_PUBLIC_KEY, MARVEL_PRIVATE_KEY);
})

.factory('jsonTextSrv', function ($http, FOLDER) {
	return  $http.get(FOLDER.JSON_TEXT + 'mock-postman.json').then(function(response) {
		return response.data;
	});
});   
