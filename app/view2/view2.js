'use strict';

angular.module('marvel-app.view2', ['ngRoute'])

.config(function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
})

.value('offset', 100)
.value('step', 20)

.controller('View2Ctrl', function($scope, marvel, jsonTextSrv, offset, step) {
	// SCOPE's var
	$scope.loadingCharacters = false;
	$scope.flagFirst = false;
	$scope.introductionText = 'Click the BUTTON to get the CHARACTERS list'

	$scope.indexMin = angular.copy(offset);
	$scope.indexMax = $scope.indexMin + step;
	// SCOPE's Fn
	$scope.changeMax = function() {
		if($scope.indexMin) { $scope.indexMax = $scope.indexMin + step;}
		else $scope.indexMax = undefined;
	}

	$scope.getNextCharacters = function() {
		$scope.indexMin += step;
		this.changeMax();
		// Fn
		this.actionCharacters($scope.indexMin);
	}

	$scope.getPreviewCharacters = function() {
		$scope.indexMin -= step;
		this.changeMax();
		// Fn
		this.actionCharacters($scope.indexMin);
	}

	$scope.actionCharacters = function(from) {
		// RUN the loader
		$scope.loadingCharacters = true;
		$scope.flagFirst = true;
// TODO : COMMENT its JUST A HACK from
/*		jsonTextSrv.then(function(JSONtext) {
			// TITRE de l'entête NAVBAR
			if($scope.indexMax < 100) $scope.results = JSONtext.data.results.slice($scope.indexMin, $scope.indexMax);

		}).finally(function() {
			$scope.loadingCharacters = false;
		});	*/
		// GET the data from marvelAPI

/// TODO : UNCOMMENT WHEN COM IS DOWN

		marvel.getCharacters(from, step)
	    	.then(function(response) {
	    		// ####################################################################################
	    		// JSON obj see https://developer.marvel.com/docs#!/public/getComicsCollection_get_6 ##
	    		// ####################################################################################
	        	if(response) $scope.results = response.results;
	        	// STOP the loader
	        	$scope.loadingCharacters = false;
	    	})
	    	.finally(function() {
	        	// console.log('something ENDING');
	    	});	
	}
})
		
.component('characterDetail', {
	templateUrl: 'view2/character-detail.html',
	controllerAs: 'vm',
	controller: function() {
		var vm = this;
		vm.toggle = function() {
        	vm.character.flagged = !vm.character.flagged;
      	}
	},
	bindings: {
		character: '=',
		item: '=?'
	}
});