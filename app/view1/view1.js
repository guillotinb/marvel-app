'use strict';

angular.module('marvel-app.view1', ['ngRoute'])

.config(function($routeProvider) {
  	$routeProvider
  		.when('/view1', {
    		templateUrl: 'view1/view1.html',
    		controller: 'View1Ctrl'
  		});
})

.controller('View1Ctrl', function($scope, marvel) {
	// DEFINE nb of results displayed
	var _step = 20;

	$scope.loadingComics = false;
	$scope.flagFirst = false;
	$scope.introductionText = 'Click the BUTTON to get the COMICS stories\' list '

	$scope.indexMin = 1;
	$scope.indexMax = $scope.indexMin + _step;

	// SCOPE's Fn
	$scope.changeMax = function() {
		if($scope.indexMin) { $scope.indexMax = $scope.indexMin + _step;}
		else $scope.indexMax = undefined;
	}

	$scope.getNextComics = function() {
		$scope.indexMin += _step;
		this.changeMax();
		// Fn
		this.actionComics($scope.indexMin);
	}

	$scope.getPreviewComics = function() {
		$scope.indexMin -= _step;
		this.changeMax();
		// Fn
		this.actionComics($scope.indexMin);
	}

	$scope.actionComics = function(from) {
		$scope.flagFirst = true;
		// RUN the loader
		$scope.loadingComics = true;
		// WE set OFFSET with minus 1 because of display convenience
		var offset = --from; // BUT we lost nb 0
		// GET the data from marvelAPI
		marvel.getComics(from, _step)
			.then(function(response) {
	    		// ####################################################################################
	    		// JSON obj see https://developer.marvel.com/docs#!/public/getComicsCollection_get_6 ##
	    		// ####################################################################################
	        	if(response) $scope.results = response.results;
	        	// STOP the loader
	        	$scope.loadingComics = false;
	    	})
	    	.finally(function() {
	        	// console.log('something ENDING');
	    	});	
	}

})
.component('comicDetail', {
	templateUrl: 'view1/comic-detail.html',
	bindings: {
		comic: '='
	}
});